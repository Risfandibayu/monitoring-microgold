<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class dashboard extends Controller
{
    //

    public function maps(){
        $maps = DB::table('users as u')
                ->select(DB::raw('REPLACE(substring_index(u.address,\'city":"\',-1) ,\'"}\',\'\') as kota'), 'u.id as id', 'c.lat', 'c.long')
                ->join('cities as c', 'c.city', 'like', DB::raw('concat(\'%\',REPLACE(substring_index(u.address,\'city":"\',-1) ,\'"}\',\'\'),\'%\')'))
                ->where('u.plan_id', '!=', 0)
                ->where('u.email', 'not like', '%+%')
                ->whereNotNull('c.lat')
                ->having('kota', '!=', '')
                ->get();

        // dd($maps);
        return response()->json(['status'=>200,'data'=>$maps,'status'=>'success']);
    }
    public function maps_login(){
        $maps = DB::table('user_logins')->where('latitude','!=',null)->where('longitude','!=',null)
        // ->whereMonth('created_at',Carbon::now()->month)
        ->whereDate('created_at', Carbon::today())
                ->get();

        // dd($maps);
        return response()->json(['status'=>200,'data'=>$maps,'status'=>'success']);
    }

    //m1
    public function tbj(){
        echo db::table('users')->where('plan_id','!=',0)->count();
    }
    public function tbjp(){
        $today = db::table('users')->where('plan_id','!=',0)->whereDate('created_at', Carbon::today())->count();
        $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();

        // $p = ($today->bro/$bro->bro*100)-100;
        if ($today == 0 || $bro == 0) {
            # code...
            $p = 0;
        }else{
            $p = $today/$bro*100;

        }
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span style='color:#2B9C26'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }

    //m2
    public function tbp(){
        echo db::table('users')->where('plan_id','!=',0)->where('email', 'not like', "%+%")->count();
    }
    public function tbpp(){
        $par = db::table('users')->where('plan_id','!=',0)->where('email', 'not like',"%+%")->whereDate('created_at', Carbon::today())->count();
                                    $bro = db::table('users')->where('plan_id','!=',0)->where('email', 'not like',"%+%")->whereDate('created_at','!=', Carbon::today())->count();
                                    // $bro = db::table('users')->where('plan_id','!=',0)->count();
                                    if ($par == 0 && $bro == 0) {
                                        # code...
                                        $p = 0;
                                    }elseif ($par != 0 && $bro == 0) {
                                        # code...
                                        $p = $par*100;
                                    }else{
                                        $p = $par/$bro*100;
                                    }
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span class='' style='color:#2B9C26'>
                        ".abs(round($p,2))."
                    </span>
                    <span id='per' class='' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,2))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }

    //m3
    public function bjt(){
        $bq = db::table('transactions')->where('remark','purchased_plan')->whereDate('created_at', Carbon::today())->select(db::raw('sum(REGEXP_SUBSTR(details,"[0-9]+")) as bro'))->first();

        return response()->json(['s' => $bq->bro ?? 0]);
    }
    public function bjtp(){
        $today = db::table('transactions')->where('remark','purchased_plan')->whereDate('created_at', Carbon::today())->select(db::raw('sum(REGEXP_SUBSTR(details,"[0-9]+")) as bro'))->first();
                                    $yesterday = db::table('transactions')->where('remark','purchased_plan')->whereDate('created_at', Carbon::yesterday())->select(db::raw('sum(REGEXP_SUBSTR(details,"[0-9]+")) as bro'))->first();
                                    // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                    if ($today->bro == 0 && $yesterday->bro == 0) {
                                        # code...
                                        $p = 0;
                                    }elseif ($today->bro != 0 && $yesterday->bro == 0) {
                                        # code...
                                        $p = $today->bro*100;
                                    }else{
                                        $p = ((($yesterday->bro - $today->bro)/$yesterday->bro)*100)* -1;

                                    }                           
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span class='' style='color:#2B9C26'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }
    
    
    //m4
    public function tdp(){
        $sg = db::table('sendgolds')->where('status',2)->count();
        $bpp = db::table('brodevs')->where(['ship_method'=>2])->where('status',2)->orwhere('ship_method',null)->where('status',2)->count();
        echo $sg + $bpp;
    }
    public function tdpp(){
        $today = db::table('sendgolds')->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count() + db::table('brodevs')->where(['ship_method'=>2])->where('status',2)->whereMonth('created_at',Carbon::now()->month)->orwhere('ship_method',null)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count();
                                    $yesterday =  db::table('sendgolds')->whereMonth('created_at',Carbon::now()->month - 1)->count() + db::table('brodevs')->where(['ship_method'=>2])->whereMonth('created_at',Carbon::now()->month - 1)->orwhere('ship_method',null)->whereMonth('created_at',Carbon::now()->month - 1)->count();
                                    // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                    if ($today == 0 || $yesterday == 0) {
                                        # code...
                                        $p = 0;
                                    }else{
                                        $p1 = ((($yesterday-$today)/$yesterday)*100);
                                        if ($p1>100) {
                                            # code...
                                            $p = $p1 - 100;
                                        }else{
                                            $p = $p1 * -1;
                                        }
                                    }
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span class='' style='color:#2B9C26'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }


    //m5
    public function dp(){
        echo db::table('sendgolds')->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count();
    }
    public function dpp(){
        $today = db::table('sendgolds')->whereMonth('created_at',Carbon::now()->month)->where('status',2)->count();
        $yesterday =  db::table('sendgolds')->whereMonth('created_at',Carbon::now()->month - 1 )->count();
        // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
        if ($today == 0 || $yesterday == 0) {
            # code...
            $p = 0;
        }else{
            // $p = $today/$yesterday*100;
            $p1 = ((($yesterday-$today)/$yesterday)*100);
            if ($p1>100) {
                # code...
                $p = $p1 - 100;
            }else{
                $p = $p1 * -1;
            }
        }     
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span class='' style='color:#2B9C26'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }

    //m6
    public function bpdp(){
        echo db::table('brodevs')->where(['ship_method'=>2])->where('status',2)->whereMonth('created_at',Carbon::now()->month)->orwhere('ship_method',null)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count();
    }
    public function bpdpp(){
        $today = db::table('brodevs')->where('ship_method',2)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->orwhere('ship_method',null)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count();
        $yesterday =  db::table('brodevs')->where('ship_method',2)->whereMonth('created_at',Carbon::now()->month-1)->orwhere('ship_method',null)->whereMonth('created_at',Carbon::now()->month-1)->count();
        // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
        if ($today == 0 || $yesterday == 0) {
            # code...
            $p = 0;
        }else{
            $p1 = ((($yesterday-$today)/$yesterday)*100);
            if ($p1>100) {
                # code...
                $p = $p1 - 100;
            }else{
                $p = $p1 * -1;
            }
        }          
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span class='' style='color:#2B9C26'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }

    //m7
    public function bppu(){
        echo db::table('brodevs')->where('ship_method',1)->where('status',2)->count();
    }
    public function bppup(){
        $today = db::table('brodevs')->where('ship_method',1)->where('status',2)->whereDate('created_at',Carbon::today())->count();
                                    $yesterday =  db::table('brodevs')->where('ship_method',1)->whereDate('created_at',Carbon::yesterday())->count();
                                    // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                    if ($today == 0 && $yesterday == 0) {
                                        # code...
                                        $p = 0;
                                    }else if ($today != 0 && $yesterday == 0) {
                                        # code...
                                        $p = $today;
                                    }else{
                                        $p1 = ((($yesterday-$today)/$yesterday)*100);
                                        if ($p1>100) {
                                            # code...
                                            $p = $p1 - 100;
                                        }else{
                                            $p = $p1 * -1;
                                        }
                                    }         
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span class='' style='color:#2B9C26'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }

    public function st(Request $request){
        $s = db::table('products')->select(db::raw('sum(stok) as s'))->where('is_custom',0)->where('weight',$request->key)->groupBy('weight')->first();

        return response()->json(['s' => $s->s]);
    }
    public function st1(Request $request){
        $s = db::table('products')->select(db::raw('sum(stok) as s'))->where('is_custom',0)->where('weight',0.01)->groupBy('weight')->first();

        return response()->json(['s' => $s->s]);
    }
    public function st2(Request $request){
        $s = db::table('products')->select(db::raw('sum(stok) as s'))->where('is_custom',0)->where('weight',0.02)->groupBy('weight')->first();

        return response()->json(['s' => $s->s]);
    }
    public function st3(Request $request){
        $s = db::table('products')->select(db::raw('sum(stok) as s'))->where('is_custom',0)->where('weight',0.05)->groupBy('weight')->first();

        return response()->json(['s' => $s->s]);
    }




    //m9
    public function ofbi(){
        echo db::table('brodevs')->where('ship_method',1)->whereMonth('created_at',Carbon::now()->month)->count();
    }
    public function ofbip(){
        $today = db::table('brodevs')->where('ship_method',1)->whereMonth('created_at',Carbon::now()->month)->count();
                                    $yesterday =  db::table('brodevs')->where('ship_method',1)->whereMonth('created_at',Carbon::now()->month - 1)->count();
                                    // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                    if ($today == 0 && $yesterday == 0) {
                                        # code...
                                        $p = 0;
                                    }else if ($today != 0 && $yesterday == 0) {
                                        # code...
                                        $p = $today;
                                    }else{
                                        $p1 = ((($yesterday-$today)/$yesterday)*100);
                                        if ($p1>100) {
                                            # code...
                                            $p = $p1 - 100;
                                        }else{
                                            $p = $p1 * -1;
                                        }
                                    }         
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span class='' style='color:#2B9C26'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }


    //m10
    public function cdbi(){
        echo db::table('corders')->whereMonth('created_at',Carbon::now()->month)->count();
    }
    public function cdbip(){
        $today = db::table('corders')->whereMonth('created_at',Carbon::now()->month)->count();
                                    $yesterday =  db::table('corders')->whereMonth('created_at',Carbon::now()->month - 1)->count();
                                    // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                    if ($today == 0 && $yesterday == 0) {
                                        # code...
                                        $p = 0;
                                    }else if ($today != 0 && $yesterday == 0) {
                                        # code...
                                        $p = $today;
                                    }else{
                                        $p1 = ((($yesterday-$today)/$yesterday)*100);
                                        if ($p1>100) {
                                            # code...
                                            $p = $p1 - 100;
                                        }else{
                                            $p = $p1 * -1;
                                        }
                                    }         
        if ($p > 0) {
            # code...
            echo "<i class='fa fa-arrow-up ' style='color:#2B9C26' aria-hidden='true' id='up'></i>
                    <span class='' style='color:#2B9C26'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='' style='color:#2B9C26'>
                        %
                    </span>";
            }else{
            echo "<i class='fa fa-arrow-down text-danger' aria-hidden='true' id='down'></i>
                    <span class='text-danger'>
                        ".abs(round($p,1))."
                    </span>
                    <span id='per' class='text-danger'>
                        %
                    </span>";

        }
    }
}
