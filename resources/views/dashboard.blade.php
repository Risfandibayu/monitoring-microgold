<?php 
    use Illuminate\Support\Facades\DB;
    use Carbon\Carbon;
    ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">
    <link rel="shortcut icon" type="image/png" href="http://localhost/microgold/assets/images/logoIcon/favicon.png">
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>MicroGold - Monitoring Page</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700,800&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">




    <!-- Theme Styles -->
    <link href="{{ asset('public') }}/css/main.min.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/dark-theme.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/custom.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        .text-x {
            font-size: 6vw
        }

        .card-header {
            font-size: 20px;
        }

        .alert {
            border-radius: 0px !important;
        }

        .card {
            border-radius: 0px 0px 15px 15px !important;
        }

        .border {
            border-style: solid;
            color: #7888fc;
        }
    </style>
</head>

<body>

    <div class="page-container">
        <div class="">

            <div class="main-wrapper">
                <div class="alert alert-success outline-alert" role="alert">
                    #BRO MONITORING
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center ">
                            <div class="card-header text-success">
                                TOTAL BRO JOINED
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-success" id="tbj">
                                    {{db::table('users')->where('plan_id','!=',0)->count()}}
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center">
                            <div class="card-header text-success">
                                TOTAL BRO (Parent)
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-success" id="tbp">
                                    {{db::table('users')->where('plan_id','!=',0)->where('email', 'not like',
                                    "%+%")->count()}}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center">
                            <div class="card-header text-success">
                                BRO JOINED TODAY
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-success" id="bjt">
                                    <?php $bq = db::table('transactions')->where('remark','purchased_plan')->whereDate('created_at', Carbon::today())->select(db::raw('sum(REGEXP_SUBSTR(details,"[0-9]+")) as bro'))->first() ?>
                                    {{-- {{db::table('brodevs')->sum('bro_qty')->whereDate('created_at',
                                    Carbon::today())->first()}} --}}
                                    {{$bq->bro}}
                                </h1>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="alert alert-danger outline-alert" role="alert">
                    #DELIVERY MONITORING
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center">
                            <div class="card-header text-danger">
                                DELIVERY RETAIL PENDING THIS MONTH
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-danger" id="dp">
                                    {{db::table('sendgolds')->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count()}}
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center">
                            <div class="card-header text-danger">
                                BRO PACK DELIVERY PENDING THIS MONTH
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-danger" id="bpdp">
                                    {{db::table('brodevs')->where(['ship_method'=>2])->where('status',2)->whereMonth('created_at',Carbon::now()->month)->orwhere('ship_method',null)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count()}}
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center">
                            <div class="card-header text-danger">
                                PICK UP PENDING
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-danger" id="bppu">
                                    {{db::table('brodevs')->where('ship_method',1)->where('status',2)->count()}}
                                </h1>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="alert alert-warning outline-alert" role="alert">
                    #STOCK MONITORING
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center">
                            <div class="card-header text-warning ">
                                STOCK 0.01 gr
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-warning" id="st1">
                                    <?php $s = db::table('products')->select(db::raw('sum(stok) as s'))->where('is_custom',0)->where('weight',0.01)->groupBy('weight')->first();?>
                                    {{$s->s}}
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center">
                            <div class="card-header text-warning">
                                STOCK 0.02 gr
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-warning" id="st2">
                                    <?php $s = db::table('products')->select(db::raw('sum(stok) as s'))->where('is_custom',0)->where('weight',0.02)->groupBy('weight')->first();?>
                                    {{$s->s}}
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="card text-center">
                            <div class="card-header text-warning">
                                STOCK 0.05 gr
                            </div>
                            <div class="card-body">
                                <h1 class="text-x text-warning" id="st3">
                                    <?php $s = db::table('products')->select(db::raw('sum(stok) as s'))->where('is_custom',0)->where('weight',0.05)->groupBy('weight')->first();?>
                                    {{$s->s}}
                                </h1>
                            </div>

                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    {{-- <script src="{{ asset('public') }}/plugins/jquery/jquery-3.4.1.min.js"></script> --}}
    <script>
        function tbj() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.tbj') }}"
        })
        .done(function( data ) {
        $('#tbj').html(data);

        setTimeout(tbj, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        tbj();
    </script>

    <script>
        function tbp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.tbp') }}"
        })
        .done(function( data ) {
        $('#tbp').html(data);

        setTimeout(tbp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        tbp();
    </script>

    <script>
        function bjt() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bjt') }}"
        })
        .done(function( data ) {
        $('#bjt').html(data.s);

        setTimeout(bjt, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bjt();
    </script>

    <script>
        function dp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.dp') }}"
        })
        .done(function( data ) {
        $('#dp').html(data);

        setTimeout(dp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        dp();
    </script>

    <script>
        function bpdp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bpdp') }}"
        })
        .done(function( data ) {
        $('#bpdp').html(data);

        setTimeout(bpdp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bpdp();
    </script>

    <script>
        function bppu() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bppu') }}"
        })
        .done(function( data ) {
        $('#bppu').html(data);

        setTimeout(bppu, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bppu();
    </script>

    <script>
        function st1() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.st1') }}"
        })
        .done(function( data ) {
        $('#st1').html(data.s);

        setTimeout(st1, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        st1();
    </script>
    <script>
        function st2() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.st2') }}"
        })
        .done(function( data ) {
        $('#st2').html(data.s);

        setTimeout(st2, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        st2();
    </script>
    <script>
        function st3() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.st3') }}"
        })
        .done(function( data ) {
        $('#st3').html(data.s);

        setTimeout(st3, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        st3();
    </script>

    {{-- @foreach ($stock as $key => $item)
    <script>
        function st{{$key}}() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.st',$item->weight) }}"
        })
        .done(function( data ) {
        $('#st'+{{$key}}).html(data.s);
        // console.log(data)

        setTimeout(st{{$key}}, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        st{{$key}}();
    </script>
    @endforeach --}}
    {{-- <script src="https://unpkg.com/@popperjs/core@2"></script>
    <script src="{{ asset('public') }}/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/feather-icons"></script>
    <script src="{{ asset('public') }}/plugins/perfectscroll/perfect-scrollbar.min.js"></script>
    <script src="{{ asset('public') }}/js/main.min.js"></script> --}}
    {{-- <script>
        console.error = function(){
        window.location.reload()
        }
    </script> --}}
    {{-- <script>
        $(document).ajaxStop(function(){
            window.location.reload();
        });
    </script> --}}
</body>

</html>