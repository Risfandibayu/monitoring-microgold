<?php 
    use Illuminate\Support\Facades\DB;
    use Carbon\Carbon;
    ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">
    <link rel="shortcut icon" type="image/png" href="http://localhost/microgold/assets/images/logoIcon/favicon.png">
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>MicroGold - Monitoring Page</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700,800&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />




    <!-- Theme Styles -->
    <link href="{{ asset('public') }}/css/main.min.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/dark-theme.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/custom.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/leaflet') }}/leaflet.css" />
    <link rel="stylesheet" href="{{ asset('public') }}/dist/MarkerCluster.css" />
    <link rel="stylesheet" href="{{ asset('public') }}/dist/MarkerCluster.Default.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        .text-x {
            font-size: 30vw
        }

        .text-y {
            font-size: 4vw
        }

        .text-z {
            font-size: 3vw
        }


        .alert {
            border-radius: 0px !important;
        }

        .card {
            border-radius: 0px 0px 15px 15px !important;
        }

        .border {
            border-style: solid;
            color: #7888fc;
        }
    </style>
</head>

<body>

    <div class="page-container">
        <div class="card text-center" style="height: 92vh">
            <div class="text-success text-z">
                Maps Clustering (Login Activity Today)
            </div>
            <div class="card-body">
                <div id="map" style="width: 100%; height: 70vh;"></div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="{{ asset('public/leaflet') }}/leaflet.js"></script>
    <script src="{{ asset('public') }}/dist/leaflet.markercluster-src.js"></script>
    <script>
        const map = L.map('map').setView([-1.9893, 118.9213], 5);

        const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);
        var markers = L.markerClusterGroup({
            singleMarkerMode: true
        }).addTo(map);
        

        $.ajax({
            url: "{{route('maps_login_data')}}",
            cache: false,
            success: function(res) {
                $.each(res.data, function(key, val) {
                    const marker = L.marker([val.latitude, val.longitude]);
                    markers.addLayer(marker);

                    // map.addLayer(markers);
                    // map.fitBounds(markers.getBounds());
                });
            }
        });

        // Function to add a random number of markers to the cluster group every second
        function updateMarkers() {
            // Make an AJAX request to the route
            var xhr = new XMLHttpRequest();
            xhr.open('GET', "{{route('maps_login_data')}}", true);
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    // Parse the response as JSON
                    var data = JSON.parse(xhr.responseText);

                    // Remove all existing markers from the cluster group
                    markers.clearLayers();

                    // Add markers to the cluster group based on the data
                    data.data.forEach(function(item) {
                        var marker = L.marker([item.latitude, item.longitude]);
                        markers.addLayer(marker);
                        // console.log(item);
                    });
                }
            };
            xhr.send();
        }

        // Call the updateMarkers function every second
        setInterval(updateMarkers, 1000);


        
    </script>

</body>

</html>