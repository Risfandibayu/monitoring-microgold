<?php 
    use Illuminate\Support\Facades\DB;
    use Carbon\Carbon;
    ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">
    <link rel="shortcut icon" type="image/png" href="http://localhost/microgold/assets/images/logoIcon/favicon.png">
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>MicroGold - Monitoring Page</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700,800&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />




    <!-- Theme Styles -->
    <link href="{{ asset('public') }}/css/main.min.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/dark-theme.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/custom.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        .text-x {
            font-size: 30vw
        }

        .text-y {
            font-size: 4vw
        }


        .alert {
            border-radius: 0px !important;
        }

        .card {
            border-radius: 0px 0px 15px 15px !important;
        }

        .border {
            border-style: solid;
            color: #7888fc;
        }
    </style>
</head>

<body>

    <div class="page-container">
        <div class="card text-center" style="height: 92vh">
            <div class="row">
                <div class="col-9 text-success text-y">
                    BRO SOLD TODAY
                </div>
                <?php 
                                    $today = db::table('transactions')->where('remark','purchased_plan')->whereDate('created_at', Carbon::today())->select(db::raw('sum(REGEXP_SUBSTR(details,"[0-9]+")) as bro'))->first();
                                    $yesterday =  db::table('transactions')->where('remark','purchased_plan')->whereDate('created_at', Carbon::yesterday())->select(db::raw('sum(REGEXP_SUBSTR(details,"[0-9]+")) as bro'))->first();
                                    // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                    if ($today->bro == 0 && $yesterday->bro == 0) {
                                        # code...
                                        $p = 0;
                                    }elseif ($today->bro != 0 && $yesterday->bro == 0) {
                                        # code...
                                        $p = $today->bro*100;
                                    }else{
                                        $p = ((($yesterday->bro - $today->bro)/$yesterday->bro)*100)* -1;

                                    }
                                    ?>
                <div class="col-3 text-y" id="bjtp">
                    <i class="fa fa-arrow-up hijau" aria-hidden="true" id="up"
                        style="display: {{$p > 0 ? '':'none'}}"></i>
                    <i class="fa fa-arrow-down text-danger" aria-hidden="true" id="down"
                        style="display: {{$p < 0 ? '':'none'}}"></i>
                    <span id="tbjp" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                        {{abs(round($p,1))}}
                    </span>
                    <span id="per" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                        %
                    </span>
                </div>
            </div>
            <hr>
            <div class="card-body">
                <h1 class="text-x text-success" id="bjt">
                    {{--
                    <?php $bq = db::table('brodevs')->select(db::raw('sum(bro_qty) as bq'))->whereDate('created_at', Carbon::today())->first() ?>
                    --}}
                    {{-- {{db::table('brodevs')->sum('bro_qty')->whereDate('created_at',
                    Carbon::today())->first()}} --}}
                    <?php $bjt = db::table('transactions')->where('remark','purchased_plan')->whereDate('created_at', Carbon::today())->select(db::raw('sum(REGEXP_SUBSTR(details,"[0-9]+")) as bro'))->first();?>
                    {{$bjt->bro ?? 0}}
                </h1>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    {{-- <script src="{{ asset('public') }}/plugins/jquery/jquery-3.4.1.min.js"></script> --}}
    <script>
        function bjt() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bjt') }}"
        })
        .done(function( data ) {
        $('#bjt').html(data.s);

        setTimeout(bjt, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bjt();
    </script>
    <script>
        function bjtp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bjtp') }}"
        })
        .done(function( data ) {
        $('#bjtp').html(data);

        setTimeout(bjtp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bjtp();
    </script>


</body>

</html>