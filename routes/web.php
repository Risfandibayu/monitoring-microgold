<?php

use App\Http\Controllers\dashboard;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $stock = db::table('products')->select(db::raw('sum(stok) as s'),'weight')->where('is_custom',0)->groupBy('weight')->get();
    return view('dashboard',compact('stock'));
});
Route::get('/m1', function () {
    return view('m1');
});
Route::get('/m2', function () {
    return view('m2');
});
Route::get('/m3', function () {
    return view('m3');
});
Route::get('/m4', function () {
    return view('m4');
});
Route::get('/m5', function () {
    return view('m5');
});
Route::get('/m6', function () {
    return view('m6');
});
Route::get('/m7', function () {
    return view('m7');
});
Route::get('/m8', function () {
    return view('m8');
});
Route::get('/m9', function () {
    return view('m9');
});
Route::get('/m10', function () {
    return view('m10');
});
Route::get('/m11', function () {
    return view('m11');
});


Route::get('/maps', function () {
    return view('maps');
});
Route::get('/maps_login', function () {
    return view('maps_login');
});

Route::get('/maps_data', [dashboard::class, 'maps'])->name('maps_data');
Route::get('/maps_login_data', [dashboard::class, 'maps_login'])->name('maps_login_data');


//m1
Route::get('/count_tbj', [dashboard::class, 'tbj'])->name('count.tbj');
Route::get('/count_tbjp', [dashboard::class, 'tbjp'])->name('count.tbjp');

//m2
Route::get('/count_tbp', [dashboard::class, 'tbp'])->name('count.tbp');
Route::get('/count_tbpp', [dashboard::class, 'tbpp'])->name('count.tbpp');

//m3
Route::get('/count_bjt', [dashboard::class, 'bjt'])->name('count.bjt');
Route::get('/count_bjtp', [dashboard::class, 'bjtp'])->name('count.bjtp');

//m4
Route::get('/count_tdp', [dashboard::class, 'tdp'])->name('count.tdp');
Route::get('/count_tdpp', [dashboard::class, 'tdpp'])->name('count.tdpp');

//m5
Route::get('/count_dp', [dashboard::class, 'dp'])->name('count.dp');
Route::get('/count_dpp', [dashboard::class, 'dpp'])->name('count.dpp');

//m6
Route::get('/count_bpdp', [dashboard::class, 'bpdp'])->name('count.bpdp');
Route::get('/count_bpdpp', [dashboard::class, 'bpdpp'])->name('count.bpdpp');


//m7
Route::get('/count_bppu', [dashboard::class, 'bppu'])->name('count.bppu');
Route::get('/count_bppup', [dashboard::class, 'bppup'])->name('count.bppup');


//m8
// $stock = db::table('products')->select(db::raw('sum(stok) as s'),'weight')->where('is_custom',0)->groupBy('weight')->get();
Route::get('/count_st/{key}', [dashboard::class, 'st'])->name('count.st');
Route::get('/count_st1', [dashboard::class, 'st1'])->name('count.st1');
Route::get('/count_st2', [dashboard::class, 'st2'])->name('count.st2');
Route::get('/count_st3', [dashboard::class, 'st3'])->name('count.st3');

//m9
Route::get('/count_ofbi', [dashboard::class, 'ofbi'])->name('count.ofbi');
Route::get('/count_ofbip', [dashboard::class, 'ofbip'])->name('count.ofbip');

//m10
Route::get('/count_cdbi', [dashboard::class, 'cdbi'])->name('count.cdbi');
Route::get('/count_cdbip', [dashboard::class, 'cdbip'])->name('count.cdbip');
